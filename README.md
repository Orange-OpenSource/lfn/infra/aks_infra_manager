Role
----

Deploy an Azure Kubernetes Service

BEWARE
------

we're following chained-ci-tools submodules. Needs to
update when this branch will get merged.


Input
-----
  - configuration files:
    - mandatory:
      - vars/pdf.yml
      - vars/idf.yml
      - vars/azure_credentials.yml: the azure credentials
      - inventory/localhost: the ansible inventory for localhost
  - Environment variables:
    - mandatory:
      - PRIVATE_TOKEN: to get the artifacts
      - artifacts_src or artifact_bin: the url to get the artifacts or
        the binary artifact
    - optional:
      - CLEAN:
          - role: trigg the clean part of the CI
          - default: ""
      - ONLY_CLEAN:
          - role: only clean the resources, do not recreate
          - default: ""
      - TENANT_NAME:
          - role: the name of the resource group
          - value type: string
          - default: the value in idf (os_infra.tenant.name).
      - USER_NAME:
          - role: the name of the user we'll set as admin user in the VMs
          - value type: string
          - default: the value in idf (os_infra.user.name).
      - IDENTIFIER:
          - role: string added to all created stuff as a tag
          - value type: string dns compatible
          - default: "by-az-infra-manager"
        - NETWORK_IDENTIFIER:
          - role: string added to all network stuff as a tag
          - value type: string dns compatible. if set to NONE, no identifier put
          - **Warning**: if set to NONE and CLEAN set, we'll try to remove
            __all__ the networks of the project which are not set to external
          - default: value of identifier
Output
------
  - artifacts:
    - inventory/infra: the inventory of the deployed infra
    - vars/pdf.yml
    - vars/idf.yml
    - vars/kube-config: the kube config file
